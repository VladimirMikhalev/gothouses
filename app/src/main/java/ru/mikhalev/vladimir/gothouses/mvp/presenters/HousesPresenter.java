package ru.mikhalev.vladimir.gothouses.mvp.presenters;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;
import ru.mikhalev.vladimir.gothouses.mvp.views.IHousesView;
import ru.mikhalev.vladimir.gothouses.utils.AppConfig;

public class HousesPresenter implements IHousesPresenter{
    private static HousesPresenter ourInstance = new HousesPresenter();
    private IHousesView mHousesView;

    public static HousesPresenter getInstance() {
        return ourInstance;
    }

    private HousesPresenter() {
    }

    @Override
    public void takeView(IHousesView housesView) {
        mHousesView = housesView;
    }

    @Override
    public void dropView() {
        mHousesView = null;
    }

    @Override
    public void initView() {
        List<String> housesNames = new ArrayList<>();
        for (Integer houseId : AppConfig.HOUSE_IDS)
            housesNames.add(DataManager.getInstance().getHouseFromDB(houseId).getName());
        mHousesView.initUI(housesNames);
    }

    @Nullable
    @Override
    public IHousesView getView() {
        return mHousesView;
    }
}
