package ru.mikhalev.vladimir.gothouses.mvp.views;


import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.data.storage.House;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.ICharacterPresenter;

public interface ICharacterView {

    ICharacterPresenter getPresenter();

    void showSnackBar(String message);

    void initUI(House house, Character character, Character mother, Character father);
}
