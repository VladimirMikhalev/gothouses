package ru.mikhalev.vladimir.gothouses.mvp.presenters;


import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.mvp.views.IHousesView;

public interface IHousesPresenter {

    void takeView(IHousesView housesView);

    void dropView();
    void initView();

    @Nullable
    IHousesView getView();
}
