package ru.mikhalev.vladimir.gothouses.mvp.views;


import java.util.List;

import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.ICharacterListPresenter;

public interface ICharacterListView {

    ICharacterListPresenter getPresenter();

    void setupCharacterList(List<Character> characters);
}
