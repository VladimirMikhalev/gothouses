package ru.mikhalev.vladimir.gothouses.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.databinding.ActivityHousesBinding;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.HousesPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.IHousesPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.views.IHousesView;
import ru.mikhalev.vladimir.gothouses.ui.adapters.HousesPagerAdapter;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;

public class HousesActivity extends AppCompatActivity
        implements ViewPager.OnPageChangeListener, IHousesView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "HousesActivity";
    private static IHousesPresenter mPresenter = HousesPresenter.getInstance();
    private ActivityHousesBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mBinding.navigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mBinding.navigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.navigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void initUI(List<String> housesNames) {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_houses);

        HousesPagerAdapter adapter = new HousesPagerAdapter(getSupportFragmentManager(), housesNames);
        mBinding.viewPager.setAdapter(adapter);
        mBinding.viewPager.addOnPageChangeListener(this);
        mBinding.tabs.setupWithViewPager(mBinding.viewPager);

        setupToolbar();
        setupDrawer(housesNames);
    }

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setupDrawer(List<String> houseNames) {
        Menu drawerMenu = mBinding.navigationView.getMenu();
        for (int i = 0; i < 3; i++) {
            MenuItem house1 = drawerMenu.getItem(i);
            house1.setTitle(houseNames.get(i));
        }

        drawerMenu.getItem(0).setChecked(true);

        mBinding.navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                mBinding.navigationDrawer.closeDrawer(GravityCompat.START);
                switch (item.getItemId()) {
                    case R.id.house1_menu:
                        mBinding.viewPager.setCurrentItem(0);
                        return true;
                    case R.id.house2_menu:
                        mBinding.viewPager.setCurrentItem(1);
                        return true;
                    case R.id.house3_menu:
                        mBinding.viewPager.setCurrentItem(2);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mBinding.navigationView.getMenu().getItem(position).setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public IHousesPresenter getPresenter() {
        return mPresenter;
    }
}
