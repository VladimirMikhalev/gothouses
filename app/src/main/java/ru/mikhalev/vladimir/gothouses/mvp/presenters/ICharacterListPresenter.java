package ru.mikhalev.vladimir.gothouses.mvp.presenters;


import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterListView;

public interface ICharacterListPresenter {

    void takeView(ICharacterListView characterListView);

    void dropView();
    void initView(int houseId);

    @Nullable
    ICharacterListView getView();
}
