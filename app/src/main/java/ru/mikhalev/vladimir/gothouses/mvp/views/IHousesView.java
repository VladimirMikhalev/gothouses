package ru.mikhalev.vladimir.gothouses.mvp.views;


import java.util.List;

import ru.mikhalev.vladimir.gothouses.mvp.presenters.IHousesPresenter;

public interface IHousesView {

    IHousesPresenter getPresenter();

    void initUI(List<String> housesNames);
}
