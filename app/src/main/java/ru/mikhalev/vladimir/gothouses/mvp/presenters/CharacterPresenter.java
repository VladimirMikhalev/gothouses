package ru.mikhalev.vladimir.gothouses.mvp.presenters;

import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;
import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.data.storage.House;
import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterView;

public class CharacterPresenter implements ICharacterPresenter{
    private static CharacterPresenter ourInstance = new CharacterPresenter();
    private ICharacterView mCharacterView;

    public static CharacterPresenter getInstance() {
        return ourInstance;
    }

    private CharacterPresenter() {
    }

    @Override
    public void takeView(ICharacterView characterView) {
        mCharacterView = characterView;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
    }

    @Override
    public void initView(int characterId) {

    }


    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }

    @Override
    public void loadData(int characterId) {
        Character character = DataManager.getInstance().getCharacterFromDB(characterId);
        House house = DataManager.getInstance().getHouseFromDB(character.getHouseId());
        Character mother = DataManager.getInstance().getCharacterFromDB(character.getMotherId());
        Character father = DataManager.getInstance().getCharacterFromDB(character.getFatherId());

        mCharacterView.initUI(house, character, mother, father);

        if (!character.getDied().isEmpty() && !character.getSeasons().isEmpty()) {
            String[] seasons = character.getSeasons().split(" ");
            String message = String.format(DataManager.getInstance().getAppContext().getString(R.string.msg_died),
                    seasons[seasons.length - 1]);
            mCharacterView.showSnackBar(message);
        }
    }
}
