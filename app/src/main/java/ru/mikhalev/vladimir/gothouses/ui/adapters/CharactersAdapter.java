package ru.mikhalev.vladimir.gothouses.ui.adapters;


import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.databinding.ItemCharacterListBinding;

public class CharactersAdapter extends BaseAdapter {

    private List<Character> mCharacters;

    public CharactersAdapter(List<Character> characters) {
        mCharacters = characters;
    }

    @Override
    public int getCount() {
        return mCharacters.size();
    }

    @Override
    public Object getItem(int position) {
        return mCharacters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        ItemCharacterListBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_character_list, parent, false);
        binding.setCharacter(mCharacters.get(position));
        return binding.getRoot();
    }
}
