package ru.mikhalev.vladimir.gothouses.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.mikhalev.vladimir.gothouses.ui.fragments.HouseMembersFragment;
import ru.mikhalev.vladimir.gothouses.utils.AppConfig;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;
import ru.mikhalev.vladimir.gothouses.utils.Helper;

public class HousesPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = ConstantManager.TAG_PREFIX + "HousesPagerAdapter";
    private List<String> mHouseNames = new ArrayList<>();

    public HousesPagerAdapter(FragmentManager fm, List<String> houseNames) {
        super(fm);
        mHouseNames = houseNames;
    }

    @Override
    public Fragment getItem(int position) {
        return HouseMembersFragment.newInstance(AppConfig.HOUSE_IDS.get(position));
    }

    @Override
    public int getCount() {
        return 3;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return Helper.getShortHouseName(mHouseNames.get(position)).toUpperCase();
    }
}
