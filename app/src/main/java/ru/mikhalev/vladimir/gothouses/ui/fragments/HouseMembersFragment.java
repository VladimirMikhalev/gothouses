package ru.mikhalev.vladimir.gothouses.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.CharacterListPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.ICharacterListPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterListView;
import ru.mikhalev.vladimir.gothouses.ui.activities.CharacterActivity;
import ru.mikhalev.vladimir.gothouses.ui.adapters.CharactersAdapter;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;

public class HouseMembersFragment extends Fragment implements ICharacterListView{

    private static final String TAG = ConstantManager.TAG_PREFIX + "HouseMembersFragment";
    private static final String ARG_HOUSE_ID = "house_id";
    private static ICharacterListPresenter mPresenter = CharacterListPresenter.getInstance();
    private ListView mListView;

    public HouseMembersFragment() {
    }

    public static HouseMembersFragment newInstance(int houseId) {

        HouseMembersFragment fragment = new HouseMembersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_HOUSE_ID, houseId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_families, container, false);
        mListView = (ListView) rootView.findViewById(R.id.characters_list);

        int houseId = getArguments().getInt(ARG_HOUSE_ID);

        mPresenter.takeView(this);
        mPresenter.initView(houseId);

        return rootView;
    }

    @Override
    public ICharacterListPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setupCharacterList(final List<Character> characters) {

        mListView.setAdapter(new CharactersAdapter(characters));

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent startCharacterActivity = new Intent(getContext(), CharacterActivity.class);
                startCharacterActivity.putExtra(ConstantManager.CHARACTER_ID, characters.get(position).getId());
                startActivity(startCharacterActivity);
            }
        });
    }
}
