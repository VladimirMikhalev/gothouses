package ru.mikhalev.vladimir.gothouses.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bhargavms.dotloader.DotLoader;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.ISplashPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.SplashPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.views.ISplashView;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;

public class SplashActivity extends AppCompatActivity implements ISplashView{
    private static final String TAG = ConstantManager.TAG_PREFIX + "SplashActivity";

    private ISplashPresenter mPresenter = SplashPresenter.getInstance();

    private CoordinatorLayout mCoordinatorLayout;
    private DotLoader mDotLoader;

    //region =========== Life cycle ============

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        mDotLoader = (DotLoader) findViewById(R.id.dot_loader);

        mPresenter.takeView(this);
        mPresenter.loadData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();

    }

    // endregion

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showInternetError() {
        Snackbar.make(mCoordinatorLayout, getString(R.string.err_msg_internet), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {
        mDotLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
        mDotLoader.setVisibility(View.GONE);
    }

    @Override
    public void startNextActivity() {
        Intent startFamiliesActivity = new Intent(this, HousesActivity.class);
        startActivity(startFamiliesActivity);
        finish();
    }

}
