package ru.mikhalev.vladimir.gothouses.mvp.presenters;


import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.mvp.views.ISplashView;

public interface ISplashPresenter {

    void takeView(ISplashView splashView);

    void dropView();
    void initView();

    @Nullable
    ISplashView getView();

    void loadData();
}
