package ru.mikhalev.vladimir.gothouses.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ru.mikhalev.vladimir.gothouses.R;
import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.data.storage.House;
import ru.mikhalev.vladimir.gothouses.databinding.ActivityCharacterBinding;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.CharacterPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.presenters.ICharacterPresenter;
import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterView;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "CharacterActivity";
    private static ICharacterPresenter mPresenter = CharacterPresenter.getInstance();

    private ActivityCharacterBinding mBinding;

    //region =========== Life cycle ============

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int characterId = getIntent().getIntExtra(ConstantManager.CHARACTER_ID, 0);
        mPresenter.takeView(this);
        mPresenter.loadData(characterId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    //endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, HousesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    //region ============ IView ===============

    @Override
    public void initUI(House house, Character character, Character mother, Character father) {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_character);
        setupToolbar();
        mBinding.setHandler(this);
        mBinding.setCharacter(character);
        mBinding.setMother(mother);
        mBinding.setFather(father);
        mBinding.setHouse(house);
    }

    @Override
    public ICharacterPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar.make(mBinding.getRoot(), message, Snackbar.LENGTH_LONG).show();
    }

    //endregion

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void startCharacter(int characterId) {
        Intent intent = getIntent();
        intent.putExtra(ConstantManager.CHARACTER_ID, characterId);
        startActivity(intent);
    }
}
