package ru.mikhalev.vladimir.gothouses.mvp.presenters;

import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;
import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterListView;

public class CharacterListPresenter implements ICharacterListPresenter{
    private static CharacterListPresenter ourInstance = new CharacterListPresenter();
    private ICharacterListView mCharacterListView;

    public static CharacterListPresenter getInstance() {
        return ourInstance;
    }

    private CharacterListPresenter() {
    }


    @Override
    public void takeView(ICharacterListView characterListView) {
        mCharacterListView = characterListView;
    }

    @Override
    public void dropView() {
        mCharacterListView = null;
    }

    @Override
    public void initView(int houseId) {
        mCharacterListView.setupCharacterList(DataManager.getInstance().getCharactersForHouse(houseId));
    }

    @Nullable
    @Override
    public ICharacterListView getView() {
        return mCharacterListView;
    }


}
