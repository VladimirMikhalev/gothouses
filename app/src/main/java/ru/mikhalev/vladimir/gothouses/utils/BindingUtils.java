package ru.mikhalev.vladimir.gothouses.utils;


import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.view.View;
import android.widget.ImageView;

import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;

public class BindingUtils {

    @BindingConversion
    public static int convertBooleanToVisibility(boolean state) {
        return state ? View.VISIBLE : View.GONE;
    }

    @BindingAdapter("setImage")
    public static void setImage(ImageView imageView, int houseId) {
        int houseIndex = AppConfig.HOUSE_IDS.indexOf(houseId);

        int imageRes = houseIndex == -1 ?
                AppConfig.DEFAULT_IMAGE :
                AppConfig.HOUSE_IMAGE_RES.get(houseIndex);
        CustomGlideModule.setImage(DataManager.getInstance().getAppContext(), imageRes, imageView);
    }

    @BindingAdapter("setIcon")
    public static void setIcon(ImageView imageView, int houseId) {
        int houseIndex = AppConfig.HOUSE_IDS.indexOf(houseId);

        int imageRes = houseIndex == -1 ?
                AppConfig.DEFAULT_ICON :
                AppConfig.HOUSE_ICON_RES.get(houseIndex);
        CustomGlideModule.setImage(DataManager.getInstance().getAppContext(), imageRes, imageView);
    }
}
