package ru.mikhalev.vladimir.gothouses.mvp.presenters;


import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;
import ru.mikhalev.vladimir.gothouses.data.network.CharacterModelResponse;
import ru.mikhalev.vladimir.gothouses.data.network.HouseModelResponse;
import ru.mikhalev.vladimir.gothouses.data.storage.Character;
import ru.mikhalev.vladimir.gothouses.data.storage.House;
import ru.mikhalev.vladimir.gothouses.mvp.views.ISplashView;
import ru.mikhalev.vladimir.gothouses.utils.AppConfig;
import ru.mikhalev.vladimir.gothouses.utils.ConstantManager;
import ru.mikhalev.vladimir.gothouses.utils.NetworkStatusChecker;

public class SplashPresenter implements ISplashPresenter {
    private static final String TAG = ConstantManager.TAG_PREFIX + "SplashPresenter";

    private static SplashPresenter ourInstance = new SplashPresenter();
    private ISplashView mSplashView;
    private DataManager mDataManager;

    private List<HouseModelResponse> mHousesResponses = new ArrayList<>();
    private List<CharacterModelResponse> mCharactersResponses = new ArrayList<>();
    private Map<String, String> mCharacterHouseMap = new HashMap<>();
    private List<House> mHouses = new ArrayList<>();
    private List<Character> mCharacters = new ArrayList<>();

    public SplashPresenter() {
        mDataManager = DataManager.getInstance();
    }

    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {}

    private void startDelay() {
        mSplashView.showLoad();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSplashView.startNextActivity();
            }
        }, AppConfig.SPLASH_DELAY);
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return null;
    }

    @Override
    public void loadData() {
        if (mDataManager.getCharactersFromDB().isEmpty()) {

            if (NetworkStatusChecker.isNetworkAvaliable()) {

                mSplashView.showLoad();
                loadHouses();
                loadCharacters();

            } else {
                mSplashView.showInternetError();
            }
        } else {
            startDelay();
        }
    }

    private void loadHouses() {
        for (int page = 1; page <= AppConfig.HOUSE_PAGES; page++) {

            Call<List<HouseModelResponse>> call = mDataManager.getHousesFromNet(page);

            call.enqueue(new Callback<List<HouseModelResponse>>() {
                @Override
                public void onResponse(Call<List<HouseModelResponse>> call, Response<List<HouseModelResponse>> response) {
                    if (response.isSuccessful()) {
                        mHousesResponses.addAll(response.body());
                    } else {
                        Log.e(TAG, "loadHouse onResponse: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<List<HouseModelResponse>> call, Throwable t) {
                    Log.e(TAG, "loadHouse onFailure: " + t.getMessage());
                }
            });
        }
    }

    private void loadCharacters() {
        for (int page = 1; page <= AppConfig.CHARACTER_PAGES; page++) {
            Call<List<CharacterModelResponse>> call = mDataManager.getCharactersFromNet(page);

            final int finalPage = page;
            call.enqueue(new Callback<List<CharacterModelResponse>>() {
                @Override
                public void onResponse(Call<List<CharacterModelResponse>> call, Response<List<CharacterModelResponse>> response) {

                    if (response.isSuccessful()) {
                        mCharactersResponses.addAll(response.body());
                        if (finalPage == AppConfig.CHARACTER_PAGES) {
                            saveData();
                        }
                    } else {
                        Log.e(TAG, "loadCharacter onResponse: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<List<CharacterModelResponse>> call, Throwable t) {
                    Log.e(TAG, "loadCharacter onFailure: " + t.getMessage());
                }
            });
        }
    }

    private void saveData() {
        for (HouseModelResponse houseModelResponse : mHousesResponses) {
            for (String characterUrl : houseModelResponse.getSwornMembers()) {
                mCharacterHouseMap.put(characterUrl, houseModelResponse.getUrl());
            }
            House house = new House(houseModelResponse);
            mHouses.add(house);
        }
        for (CharacterModelResponse characterModelResponse : mCharactersResponses) {
            String houseUrl = mCharacterHouseMap.get(characterModelResponse.getUrl());
            Character character = new Character(characterModelResponse, houseUrl);
            mCharacters.add(character);

        }
        mDataManager.saveHousesInDB(mHouses);
        mDataManager.saveCharactersInDB(mCharacters);

        mSplashView.startNextActivity();
    }
}
