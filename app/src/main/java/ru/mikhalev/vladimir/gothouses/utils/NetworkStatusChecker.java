package ru.mikhalev.vladimir.gothouses.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import ru.mikhalev.vladimir.gothouses.data.managers.DataManager;

public class NetworkStatusChecker {
    public static boolean isNetworkAvaliable() {
        ConnectivityManager cm = (ConnectivityManager)
                DataManager.getInstance().getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
