package ru.mikhalev.vladimir.gothouses.mvp.views;


import ru.mikhalev.vladimir.gothouses.mvp.presenters.ISplashPresenter;

public interface ISplashView {

    ISplashPresenter getPresenter();

    void showInternetError();

    void showLoad();

    void hideLoad();

    void startNextActivity();
}
