package ru.mikhalev.vladimir.gothouses.mvp.presenters;


import android.support.annotation.Nullable;

import ru.mikhalev.vladimir.gothouses.mvp.views.ICharacterView;

public interface ICharacterPresenter {

    void takeView(ICharacterView characterView);

    void dropView();
    void initView(int characterId);

    @Nullable
    ICharacterView getView();

    void loadData(int characterId);
}
